<?php

namespace App\Controllers;

use App\Models\Model_Employee;

class Employee extends BaseController
{
    protected $Model_Employee;
    public function __construct()
    {
        $this->Model_Employee = new Model_Employee();
    }
    public function index()
    {
        $employee = $this->Model_Employee->where('flag_delete', 0)->findAll();
        $data = [
            'employee' => $employee
        ];

        return view('employee/index', $data);
    }

    public function add()
    {

        $data = [
            'validation' => \Config\Services::validation()
        ];
        return view('employee/add', $data);
    }

    public function save()
    {
        if (!$this->validate([
            'name'  => [
                'rules'     => 'required',
                'errors'    => [
                    'required' => '{field} cannot be empty'
                ]
            ],
            'email' =>  [
                'rules'     => 'required',
                'errors'    => [
                    'required' => '{field} cannot be empty'
                ]
            ]
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/employee/add')->withInput()->with('validation', $validation);
        }
        $this->Model_Employee->save([
            'name'          => $this->request->getVar('name'),
            'email'         => $this->request->getVar('email'),
            'field'         => $this->request->getVar('field'),
            'address'       => $this->request->getVar('address'),
            'flag_delete'   => 0
        ]);
        session()->setFlashdata('message', 'Data successfully added!');

        return redirect()->to('/');
    }

    public function delete($id)
    {
        // $data = [
        //     'flag_delete' => 1
        // ];

        $this->Model_Employee->update($id, ['flag_delete' => 1]);
        session()->setFlashdata('message', 'Data successfully deleted!');

        return redirect()->to('/');
    }

    public function edit($id)
    {

        $data['employee'] = $this->Model_Employee->where(['flag_delete' => 0, 'id' => $id])->findAll();

        return view('employee/edit', $data);
    }

    public function update($id)
    {

        $this->Model_Employee->update(
            $id,
            [
                'name'         => $this->request->getVar('name'),
                'email'         => $this->request->getVar('email'),
                'field'         => $this->request->getVar('field'),
                'address'       => $this->request->getVar('address'),
                'flag_delete'   => 0
            ]
        );

        session()->setFlashdata('message', 'Data successfully edited!');

        return redirect()->to('/');
    }
}
