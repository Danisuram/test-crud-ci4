<?php

namespace App\Models;

use CodeIgniter\Model;

class Model_Employee extends Model
{
    protected $table = "employee";
    protected $primaryKey = "id";
    protected $allowedFields = ['name', 'email', 'field', 'address', 'flag_delete'];
}
